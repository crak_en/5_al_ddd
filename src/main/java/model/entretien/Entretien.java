package model.entretien;

import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

public class Entretien implements IEntretien{

    private Date dateEntretien;
    private ConsultantRecruteur consultantRecruteur;
    private EventRdv eventRdv;
    private Candidat candidat;


    public Entretien(Candidat candidat, ArrayList<ConsultantRecruteur> consultantRecruteur, ArrayList<Date> dateDispo) {
        this.dateEntretien = dateEntretien;


        this.candidat = candidat;
        candidat.setDisponibilités(dateDispo);
        setEventRdvFromDispoAndSpeciality(consultantRecruteur);


    }

    public Entretien() { }


    public void setEventRdvFromDispoAndSpeciality(ArrayList<ConsultantRecruteur> consultantRecruteurs){

        for(ConsultantRecruteur consultantRecruteur : consultantRecruteurs){
            for(String specialiteRecruteur : consultantRecruteur.getSpecialite()){
                specialiteRecruteur = specialiteRecruteur.toLowerCase();
                if(candidat.getSpecialiteCandidat().contains(specialiteRecruteur)){
                    for(Date date : consultantRecruteur.getDisponibilite()){
                        if(candidat.getDisponibilites().contains(date)){
                            this.setEventRdv(new EventRdv(date, new Double(1.0) , new Double(20.0)));
                            this.setConsultantRecruteur(consultantRecruteur);
                            return;
                        }
                    }
                }
            }
        }
    }

    public void setConsultantRecruteur() {
        new ConsultantRecruteur(candidat.getSpecialiteCandidat());
    }

    public EventRdv getEventRdv() {
        return eventRdv;
    }

    public void setEventRdv(EventRdv eventRdv) {
        this.eventRdv = eventRdv;
    }

    public Candidat getCandidat() {
        return candidat;
    }

    public void setCandidat(Candidat candidat) {
        this.candidat = candidat;
    }

    public ConsultantRecruteur getConsultantRecruteur() {
        return consultantRecruteur;
    }

    public void setConsultantRecruteur(ConsultantRecruteur consultantRecruteur) {
        this.consultantRecruteur = consultantRecruteur;
    }

    public Date getDateEntretien() {
        return dateEntretien;
    }

    public void setDateEntretien(Date dateEntretien) {
        this.dateEntretien = dateEntretien;
    }




    @Override
    public String toString() {
        return "Entretien{" +
                "dateEntretien=" + dateEntretien +
                ", eventRdv=" + eventRdv +
                ", candidat=" + candidat +
                ", consultantRecruteur=" + consultantRecruteur +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Entretien entretien = (Entretien) o;
        return Objects.equals(dateEntretien, entretien.dateEntretien) &&
                Objects.equals(consultantRecruteur, entretien.consultantRecruteur) &&
                Objects.equals(eventRdv, entretien.eventRdv) &&
                Objects.equals(candidat, entretien.candidat);
    }

    @Override
    public int hashCode() {

        return Objects.hash(dateEntretien, consultantRecruteur, eventRdv, candidat);
    }
}
