package use_cases.entretien;

import model.entretien.*;

import java.util.ArrayList;
import java.util.Date;

public class Replanification implements IFakeData {

    public Entretien modifierDisponibilite(Entretien entretien) throws Exception{
        ArrayList<ConsultantRecruteur> generateConsultants = generateConsultants();
        ArrayList<Date> dispCandidat = generateDispoCandidat();
        return new Entretien(entretien.getCandidat(), generateConsultants, generateDispoCandidat());

    }

    public ArrayList<ConsultantRecruteur> generateConsultants() {
        return new FakeData().generateConsultants();
    }

    public ArrayList<Date> generateDispoCandidat() {
        return new FakeData().generateDispoCandidat();
    }
}
