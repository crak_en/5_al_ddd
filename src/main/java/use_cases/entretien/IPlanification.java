package use_cases.entretien;

import model.entretien.Entretien;

import java.util.ArrayList;
import java.util.Date;

public interface IPlanification {

    public Entretien planifierRdv(String candidat, ArrayList<String> specialite,Integer anneeExp, Date dateRdv) throws Exception;


}
