package use_cases.entretien;

import model.entretien.Entretien;
import model.entretien.EventRdv;

public interface IAnnulation {
    public Entretien annulation(Entretien entretien) throws Exception;
}
