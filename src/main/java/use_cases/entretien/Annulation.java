package use_cases.entretien;

import model.entretien.Entretien;
import model.entretien.EventRdv;

public class Annulation implements IAnnulation {


    public Entretien annulation(Entretien entretien) throws Exception {
        entretien.getEventRdv().setActive(false);
        return  entretien;
    }


}
