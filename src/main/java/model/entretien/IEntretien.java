package model.entretien;

import java.util.ArrayList;
import java.util.Date;

public interface IEntretien {
   void setEventRdvFromDispoAndSpeciality(
      ArrayList<ConsultantRecruteur> consultantRecruteurs);

    boolean equals(Object o);
    int hashCode();

    //getter/setter
    void setConsultantRecruteur();
    EventRdv getEventRdv();
    void setEventRdv(EventRdv eventRdv);
    Candidat getCandidat();
    void setCandidat(Candidat candidat);
    ConsultantRecruteur getConsultantRecruteur();
    void setConsultantRecruteur(ConsultantRecruteur consultantRecruteur);
    Date getDateEntretien();
    void setDateEntretien(Date dateEntretien);

    }

