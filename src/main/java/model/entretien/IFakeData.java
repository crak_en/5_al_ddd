package model.entretien;

import java.util.ArrayList;
import java.util.Date;

public interface IFakeData {
    ArrayList<ConsultantRecruteur> generateConsultants();

    ArrayList<Date> generateDispoCandidat();
}
