import model.entretien.Candidat;
import model.entretien.Entretien;
import model.entretien.EventRdv;
import model.entretien.FakeData;
import org.junit.Assert;
import org.junit.Test;
import use_cases.entretien.Annulation;
import use_cases.entretien.Planification;
import use_cases.entretien.Replanification;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class TestClass {

    @Test
    public void absenceDeReponseCandidate() {
        try{
            PlanficationAbsDeReponse absDeReponse = new PlanficationAbsDeReponse();
            absDeReponse.planifierRdv("t",  null,null, new Date());
        }catch (Exception e){
            Assert.assertEquals("Sans reponse depuis 10 jours", e.getMessage());
            return;
        }
        Assert.fail("Should fail");
    }

    @Test
    public void demanderInteretCandidat() {
        try{
            AbsInteretCandidat absDeReponse = new AbsInteretCandidat();
            absDeReponse.planifierRdv("t",  null,null, new Date());
        }catch (Exception e){
            Assert.assertEquals("Absence d'interet du candidat", e.getMessage());
            return;
        }
        Assert.fail("Should fail");
    }

    @Test
    public void planifierRdv() {
        Planification planification = new Planification();
        ArrayList<String> spe = new ArrayList<String>();
        spe.add("java");

        try {
            Entretien entretien = planification.planifierRdv("test", spe, new Integer(1), new Date());
            Assert.assertEquals(entretien.getConsultantRecruteur().getName(), "Bob");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void noPersonForTechicalTestForCandidatStack() {
        try{
            NoPersonForTechicalTestForCandidatStack absDeReponse = new NoPersonForTechicalTestForCandidatStack();
            absDeReponse.planifierRdv("t", new Date());
        }catch (Exception e){
            Assert.assertEquals("Not person available", e.getMessage());
            return;
        }
        Assert.fail("Should fail");
    }
    @Test
    public void indispoCandidat(){
        try{
            FakeCandidatIndispo fakeCandidatIndispo = new FakeCandidatIndispo();
            fakeCandidatIndispo.planifierRdv("jose",  null,null, new Date());
        }catch (Exception e){
            Assert.assertEquals("Candidat Indisponible", e.getMessage());
            return;
        }
        Assert.fail("Should fail");
    }

    @Test
    public void RhIndispo() {
        try{
            FakeRhIndispo fakeRhIndispo = new FakeRhIndispo();
            fakeRhIndispo.planifierRdv("josephine", null,null, new Date());
        }catch (Exception e){
            Assert.assertEquals("RH Indisponible", e.getMessage());
            return;
        }
        Assert.fail("Should fail");
    }

    @Test
    public void TimeOut(){
        try{
            FakeTimeOut fakeTimeOut = new FakeTimeOut();
            fakeTimeOut.planifierRdv("anthony",  null,null, new Date());
        }catch (Exception e){
            Assert.assertEquals("Pas de réponse depuis 10 Jours", e.getMessage());
            return;
        }
        Assert.fail("Should fail");
    }

  @Test
  public void DateInvalid() {
    try {
      DateInvalid dateInvalid = new DateInvalid();
      dateInvalid.planifierRdv("t", null,null, null);
    } catch (Exception e) {
      Assert.assertEquals("Date null", e.getMessage());
      return;
    }
    Assert.fail("Should fail"); }

   @Test
    public void NameNull(){
        try{
            FakeNameNull fakeNameNull = new FakeNameNull();
            fakeNameNull.planifierRdv("",  null,null, new Date());
        }catch (Exception e){
            Assert.assertEquals("Pas de Nom", e.getMessage());
            return;
        }
        Assert.fail("Should fail");

    }

    @Test
    public void annulationRdv() {
        try{
            Annulation annulation = new Annulation();
            Entretien entretien = new Entretien();
            Entretien entretienAnnuler = annulation.annulation(entretien);
            Assert.assertFalse(entretienAnnuler.getEventRdv().isActive());
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

    }

    @Test
    public void replanificationRdv() {
        Replanification replanification = new Replanification();
        ArrayList<String> spe = new ArrayList<String>();
                spe.add("java");
        Candidat candidat = new Candidat("test",spe, new Integer(1));
        FakeData fakeData = new FakeData();
        ArrayList<Date> dateDispo = new ArrayList<Date>();
        dateDispo.add(new Date(15));

        Entretien entretien = new Entretien(candidat, fakeData.generateConsultants(), dateDispo);
        try {
            Entretien entretienBis = replanification.modifierDisponibilite(entretien);
            Assert.assertEquals(entretien.getConsultantRecruteur().getName(), "Bob");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void setEventRdv() {
        FakeData api = new FakeData();
        Entretien entretien = new Entretien(new Candidat("Willy", new ArrayList<String>(Arrays.asList("java")), new Integer(3)), api.generateConsultants(), api.generateDispoCandidat());
        Assert.assertNotNull(entretien.getEventRdv());

    }

    @Test
    public void DiffEntretien(){
        Planification planification = new Planification();
        ArrayList<String> spe = new ArrayList<String>();
        spe.add("java");
        Planification p2 = new Planification();
        ArrayList<String> enter = new ArrayList<String>();
        enter.add("python");
        try {
            Entretien entretien = planification.planifierRdv("test", spe, new Integer(1), new Date());
            Entretien entretien2 = p2.planifierRdv("azerty", enter, new Integer(1), new Date());
            Assert.assertFalse(entretien.equals(entretien2));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void sameEventrdv(){
        EventRdv e1 = new EventRdv(new Date(), new Double(1.0) , new Double(20.0));
        EventRdv e2 = new EventRdv(new Date(), new Double(1.0) , new Double(20.0));
        Assert.assertTrue(e1.equals(e2));

    }

}