package model.entretien;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class FakeData implements IFakeData {

    private ArrayList<ConsultantRecruteur> consultantRecruteurs = new ArrayList<ConsultantRecruteur>();

    public ArrayList<ConsultantRecruteur> generateConsultants(){

        this.consultantRecruteurs.add(new ConsultantRecruteur("Bob", new ArrayList<String>(Arrays.asList("java", "C++", "js")), new ArrayList<Date>(Arrays.asList(new Date(15)))));
        this.consultantRecruteurs.add(new ConsultantRecruteur("Georges", new ArrayList<String>(Arrays.asList("c#", "c", "python")), new ArrayList<Date>(Arrays.asList(new Date(46546)))));
        this.consultantRecruteurs.add(new ConsultantRecruteur("Laurent", new ArrayList<String>(Arrays.asList("c++", "rrlang", "rubis")), new ArrayList<Date>(Arrays.asList(new Date(789)))));
        this.consultantRecruteurs.add(new ConsultantRecruteur("Laurent", new ArrayList<String>(Arrays.asList("js", "php", "python")), new ArrayList<Date>(Arrays.asList(new Date(666)))));

        return consultantRecruteurs;
    }

    public ArrayList<Date> generateDispoCandidat() {

        ArrayList<Date> date = new ArrayList<Date>();
        date.add(new Date(15));
        date.add(new Date(666));
        date.add(new Date(789));
        date.add(new Date(46546));

        return date;
    }


}
