package use_cases.entretien;

import model.entretien.*;

import java.util.ArrayList;
import java.util.Date;

public class  Planification implements IPlanification, IFakeData {


    public Entretien planifierRdv(String candidat, ArrayList<String> specialite, Integer anneeExp, Date dateRdv) throws Exception {

       return new Entretien(new Candidat(candidat, specialite, anneeExp), generateConsultants(), generateDispoCandidat());
    }


    public ArrayList<ConsultantRecruteur> generateConsultants() {

        return new FakeData().generateConsultants();
    }

    public ArrayList<Date> generateDispoCandidat() {
        return new FakeData().generateDispoCandidat();
    }


}
