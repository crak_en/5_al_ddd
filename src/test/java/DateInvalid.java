import model.entretien.Entretien;
import model.entretien.EventRdv;

import java.util.ArrayList;
import java.util.Date;

public class DateInvalid {
    public Entretien planifierRdv(String candidat, ArrayList<String> specialite, Integer anneeExp, Date dateRdv) throws Exception {
        if(dateRdv == null){
            throw new Exception("Date null");
        }
        else{
            return new Entretien();
        }
    }
}
