package model.entretien;

import java.util.Date;
import java.util.Objects;

public class EventRdv {


    private boolean isActive;
    private Date dateRdv;
    private Double dureeEventRdv;
    private Double heureRdv;

    public Double getDureeEventRdv() {
        return dureeEventRdv;
    }

    public void setDureeEventRdv(Double dureeEventRdv) {
        this.dureeEventRdv = dureeEventRdv;
    }

    public Double getHeureRdv() {
        return heureRdv;
    }

    public void setHeureRdv(Double heureRdv) {
        this.heureRdv = heureRdv;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public EventRdv( Date dateRdv, Double dureeEventRdv,Double heureRdv ){

        setDateRdv(dateRdv);
        setActive(true);
        setDureeEventRdv(dureeEventRdv);
        setHeureRdv(dureeEventRdv);
    }

    public EventRdv(Candidat candidat){

    }

    public Date getDateRdv() {
        return dateRdv;
    }

    public void setDateRdv(Date dateRdv) {
        this.dateRdv = dateRdv;
    }

    @Override
    public String toString() {
        return "EventRdv{" +
                ", isActive=" + isActive +
                ", dateRdv=" + dateRdv +
                ", dureeEventRdv=" + dureeEventRdv +
                ", heureRdv=" + heureRdv +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EventRdv eventRdv = (EventRdv) o;
        return isActive == eventRdv.isActive &&
                Objects.equals(dateRdv, eventRdv.dateRdv) &&
                Objects.equals(dureeEventRdv, eventRdv.dureeEventRdv) &&
                Objects.equals(heureRdv, eventRdv.heureRdv);
    }

    @Override
    public int hashCode() {

        return Objects.hash(isActive, dateRdv, dureeEventRdv, heureRdv);
    }
}
