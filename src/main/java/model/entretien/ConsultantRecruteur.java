package model.entretien;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class ConsultantRecruteur {

    private String id;
    private String name;
    private ArrayList<String> Specialite;
    private ArrayList<Date> Disponibilite;
    private int minSalary;

    public ConsultantRecruteur(String name, ArrayList<String> specialite, ArrayList<Date> disponibilite) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.Specialite = specialite;
        this.Disponibilite = disponibilite;
        this.minSalary = getMinSalary(specialite);
    }

    public ConsultantRecruteur(ArrayList<String> specialiteCandidat) {
    }

    public int getMinSalary(ArrayList specialite){


        if(specialite.contains("java") && specialite.contains("javascript")){
            return 500000;
        }
        else if(specialite.contains("java")){
            return 400000;
        }
        else{
            return 300000;
        }

    }
    public String getName() {
        return name;
    }


    public ArrayList<String> getSpecialite() {
        return Specialite;
    }



    public ArrayList<Date> getDisponibilite() {
        return Disponibilite;
    }





}
