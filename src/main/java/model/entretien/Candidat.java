package model.entretien;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class Candidat {

    private String id;
    private String nomCandidat;
    private ArrayList<String> specialiteCandidat;
    private Integer anneesExperienceCandidat;
    private ArrayList<Date> disponibilites;
    private boolean isElligible;

    public ArrayList<Date> getDisponibilites() {
        return disponibilites;
    }

    public void setDisponibilités(ArrayList<Date> disponibilites) {
        this.disponibilites = disponibilites;
    }


    public Candidat(String nomCandidat, ArrayList<String> specialiteCandidat, Integer anneesExperienceCandidat ){
        this.id = UUID.randomUUID().toString();
        this.anneesExperienceCandidat = anneesExperienceCandidat.valueOf(anneesExperienceCandidat);
        this.specialiteCandidat=specialiteCandidat;
        this.nomCandidat = nomCandidat;
        this.isElligible = isEligibleCandidat(specialiteCandidat,anneesExperienceCandidat );

    }
    public Candidat(String nomCandidat){

        this.nomCandidat = nomCandidat;
    }

    public boolean isEligibleCandidat(ArrayList specialite, int anneesExperienceCandidat){


        if(specialite.contains("java") && anneesExperienceCandidat > 2){
            return true;
        }
        return false;
    }

    public String getNomCandidat() {
        return nomCandidat;
    }



    public ArrayList<String> getSpecialiteCandidat() {
        return specialiteCandidat;
    }



    public Integer getAnneesExperienceCandidat() {
        return anneesExperienceCandidat;
    }



    @Override
    public String toString() {
        return "Candidat{" +
                "nomCandidat='" + nomCandidat + '\'' +
                ", specialiteCandidat=" + specialiteCandidat +
                ", anneesExperienceCandidat=" + anneesExperienceCandidat +
                '}';
    }


}
